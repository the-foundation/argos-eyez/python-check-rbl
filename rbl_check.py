

"""
The guys who made python such a pain , especially making vw and v3 completely incompatible
shall be crucified , burned alive and sent to hell

Python is not a programming language, it is a headache and agression generator

"""

import random
import os
import requests
import socket

import requests

urllib_ver2=0
try:
    from urllib.parse import urlparse, urlencode
    from urllib.request import urlopen, Request,build_opener
    from urllib.error import HTTPError
    import urllib
except ImportError:
    import urllib2
    from urlparse import urlparse
    from urllib import urlencode
    from urllib2 import urlopen, Request, HTTPError
    urllib_ver2=1

#import urllib3.request
#import urllib3

import argparse
from threading import Thread
import dns.resolver
from dns import reversename
import functools
import re
import datetime
import pytz
from datetime import date
import time
from multiprocessing.dummy import Pool as ThreadPool
pool = ThreadPool(6)
import scrape_rbl as rblscrape

rbls=rblscrape.get_rbl()

rbls_ipv4=rblscrape.get_rbl_ipv4()
rbls_ipv6=rblscrape.get_rbl_ipv6()
rbls_domain=rblscrape.get_rbl_domain()

import user_agent_list as useragents

#print(random.choice(useragents.user_agent_list))

utc=pytz.UTC
from dateutil.parser import parse as parsedate

timeout_time = 7

global_debug=0

##base64's `urlsafe_b64encode` uses '=' as padding.
##These are not URL safe when used in URL paramaters.
##Functions below work around this to strip/add back in padding.
##See:
##https://docs.python.org/2/library/base64.html
##https://mail.python.org/pipermail/python-bugs-list/2007-February/037195.html

import base64


def base64_encode(string):
    """
    Removes any `=` used as padding from the encoded string.
    """
    bytez = string.encode("utf-8")
    encoded=base64.urlsafe_b64encode(bytez).decode("utf-8")
    return encoded.rstrip("=")

def base64_decode(string):
    """
    Adds back in the required padding before decoding.
    """
    padding = 4 - (len(string) % 4)
    string = string + ("=" * padding)
    return base64.urlsafe_b64decode(string)

def validate_ipv4(s):
    a = s.split('.')
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True


import ipaddress

def validate_ipv6_host_public(address):
    #is_ipv6=False;
    try:
        addr = ipaddress.IPv6Address(address)
    except ipaddress.AddressValueError:
        is_ip6=False
        if global_debug==1:
               print(address, 'is not a valid IPv6 address')
    else:
        if addr.is_multicast:
            is_ip6=False
            if global_debug==1:
               print(address, 'is an IPv6 multicast address')
        if addr.is_private:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 private address')
        if addr.is_global:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 global address')
        if addr.is_link_local:
            is_ip6=False
            if global_debug==1:
               print(address, 'is an IPv6 link-local address')
        if addr.is_site_local:
            is_ip6=False
            if global_debug==1:
               print(address, 'is an IPv6 site-local address')
        if addr.is_reserved:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 reserved address')
        if addr.is_loopback:
            is_ip6=False
            if global_debug==1:
               print(address, 'is an IPv6 loopback address')
        if addr.ipv4_mapped:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 mapped IPv4 address')
        if addr.sixtofour:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 RFC 3056 address')
        if addr.teredo:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 RFC 4380 address')
    return is_ip6

import ipaddress

def validate_ipv6(address):
    #is_ipv6=False;
    try:
        addr = ipaddress.IPv6Address(address)
    except ipaddress.AddressValueError:
        is_ip6=False
        if global_debug==1:
               print(address, 'is not a valid IPv6 address')
    else:
        if addr.is_multicast:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 multicast address')
        if addr.is_private:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 private address')
        if addr.is_global:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 global address')
        if addr.is_link_local:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 link-local address')
        if addr.is_site_local:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 site-local address')
        if addr.is_reserved:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 reserved address')
        if addr.is_loopback:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 loopback address')
        if addr.ipv4_mapped:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 mapped IPv4 address')
        if addr.sixtofour:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 RFC 3056 address')
        if addr.teredo:
            is_ip6=True
            if global_debug==1:
               print(address, 'is an IPv6 RFC 4380 address')
    return is_ip6


################


#### tor@dan.me.uk ###
##        e.g.   123.2.0.192.torexit.dan.me.uk
##
##    To query an IPv6 address, you must expand it, then reverse it into "nibble" format.
##    e.g. if the IP was 2001:db8::1, you expand it to 2001:0db8:0000:0000:0000:0000:0000:0001 and reverse it.
##    In nibble format it is 1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.8.b.d.0.1.0.0.2 and add on the dns blacklist you require.
##
##        e.g.   1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.8.b.d.0.1.0.0.2.tor.dan.me.uk


###tor 
#####  How can I query the public TorDNSEL service?
#####  Using the command line tool dig, users can ask type 1 queries like so:
#####  dig 209.137.169.81.6667.4.3.2.1.ip-port.exitlist.torproject.org
#####  What do the received answers mean?
#####    A request for the A record "209.137.169.81.6667.4.3.2.1.ip-port.exitlist.torproject.org" would return 127.0.0.2 if there's a Tor node that can exit through 81.169.137.209 to port 6667 at 1.2.3.4. If there isn't such an exit node, the DNSEL returns NXDOMAIN.
#####  Other A records inside net 127/8, except 127.0.0.1, are reserved for future use and should be interpreted by clients as indicating an exit node. Queries outside the DNSEL's zone of authority result in REFUSED. Ill-formed queries inside its zone of authority result in NXDOMAIN.
#####  How do I configure software with DNSBL support?
#####  Users of software with built-in support for DNSBLs can configure the following zone as a DNSBL:
#####  [service port].[reversed service
#####      address].ip-port.exitlist.torproject.org

URLS = [
#D    #TOR
#E    ('http://torstatus.blutmagie.de/ip_list_exit.php/Tor_ip_list_EXIT.csv',
#F     'is not a TOR Exit Node',
#E     'is a TOR Exit Node',
#C     False),
#T

    #dshield
    ('https://www.dshield.org/ipsascii.html?limit=10000',
     'is not listed on dshield',
     'is listed on dshield',
     True),

    #EmergingThreats
    ('https://rules.emergingthreats.net/blockrules/compromised-ips.txt',
     'is not listed on EmergingThreats',
     'is listed on EmergingThreats',
     True),

    #AlienVault
    ('https://reputation.alienvault.com/reputation.data',
     'is not listed on AlienVault',
     'is listed on AlienVault',
     True),

    #BlocklistDE
    ('https://www.blocklist.de/lists/bruteforcelogin.txt',
     'is not listed on BlocklistDE',
     'is listed on BlocklistDE',
     True),

#    #Dragon Research Group - SSH
#    ('http://dragonresearchgroup.org/insight/sshpwauth.txt',
#     'is not listed on Dragon Research Group - SSH',
#     'is listed on Dragon Research Group - SSH',
#     True),

#    #Dragon Research Group - VNC
#    ('http://dragonresearchgroup.org/insight/vncprobe.txt',
#     'is not listed on Dragon Research Group - VNC',
#     'is listed on Dragon Research Group - VNC',
#     True),

#    #NoThinkMalware
#    ('http://www.nothink.org/blacklist/blacklist_malware_http.txt',
#     'is not listed on NoThink Malware',
#     'is listed on NoThink Malware',
#     True),

#    #NoThinkSSH
#    ('http://www.nothink.org/blacklist/blacklist_ssh_all.txt',
#     'is not listed on NoThink SSH',
#     'is listed on NoThink SSH',
#     True),

    #Feodo
    ('http://rules.emergingthreats.net/blockrules/compromised-ips.txt',
     'is not listed on Feodo',
     'is listed on Feodo',
     True),

#    #antispam.imp.ch
#    ('https://antispam.imp.ch/spamlist',
##     'is not listed on antispam.imp.ch',
#     'is listed on antispam.imp.ch',
#     True),


#    #malc0de # frozen 2019-12-16
#    ('https://malc0de.com/bl/IP_Blacklist.txt',
#     'is not listed on malc0de',
#     'is listed on malc0de',
#     True),

#    #MalWareBytes
#    ('http://hosts-file.net/rss.asp',
#     'is not listed on MalWareBytes',
#     'is listed on MalWareBytes',
#     True)

#    #Spamhaus DROP (in CIDR format, needs parsing)
#    ('https://www.spamhaus.org/drop/drop.txt',
#     'is not listed on Spamhaus DROP',
#     'is listed on Spamhaus DROP',
#     False),
#    #Spamhaus EDROP (in CIDR format, needs parsing)
#    ('https://www.spamhaus.org/drop/edrop.txt',
#     'is not listed on Spamhaus EDROP',
#     'is listed on Spamhaus EDROP',
#     False)]

    ]


def timeout(timeout):
    def decor(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            res = [None]
            def newFunc():
                res[0] = func(*args, **kwargs)
            t = Thread(target=newFunc)
            t.daemon = True
            t.start()
            t.join(timeout)
            return res[0]
        return wrapper
    return decor

@timeout(timeout_time)
def _check_rbl(reverse_ip, rbl):
    try:
        myResolver = dns.resolver.Resolver()
        #myResolver.nameservers = ['9.9.9.9', '4.2.2.4']
        #socket.getaddrinfo(reverse_ip + '.' + rbl, 25)
        myAnswers = myResolver.query(reverse_ip + '.' + rbl, "A")
        if myAnswers.rrset is not None:
            if global_debug==1:
                print(" GOT RBL response ",myAnswers.rrset)
            for answer in myAnswers:
                if (not answer.to_text() == "127.0.0.1" ) and (not answer.to_text() == "0.0.0.0" )  :
                    if global_debug==1:
                        print (" dns answer: ",answer.to_text())
                    return true;
        else:
            return False
    except :
        return False

## http tests might take up to 19s  on e.g. dshield
#@timeout(15)
def _content_test(url, badip):
    """
    Test the content of url's response to see if it contains badip.
        Args:
            url -- the URL to request data from
            badip -- the IP address in question
        Returns:
            Boolean
    """
#request = urllib3.Request('http://www.example.com', data)
#response = urllib3.urlopen(request, timeout=4)
    statuscode=0
    retcode=0
    dstFile='/tmp/.rbl_cache_' + base64_encode(url)
    ten_minutes_ago =  pytz.utc.localize(datetime.datetime.fromtimestamp(time.mktime(time.localtime(time.time()-600))))
    try:
        file_time_pre = pytz.utc.localize(datetime.datetime.fromtimestamp(os.path.getmtime(dstFile)))
    except: 
        ## no file , the date is 1970-01-01
        file_time_pre=pytz.utc.localize(datetime.datetime.fromtimestamp(time.mktime(time.localtime(0))))
        pass
    if global_debug==1:
        ### we have time-offset aware objects like this : datetime.datetime(2020, 4, 21, 4, 30, 6, tzinfo=tzutc())
        print ("cache_time_pre: threshold :" , ten_minutes_ago.strftime("%d-%b-%Y (%H:%M:%S.%f)") , " local",file_time_pre.strftime("%d-%b-%Y (%H:%M:%S.%f)"))

    try:
        if ( file_time_pre < ten_minutes_ago ): ## cached file not present or older than 10 m
            #    
            r = requests.head(url, headers={'User-Agent': random.choice(useragents.user_agent_list) })
            try:
                url_time = r.headers['last-modified']
                url_date = parsedate(url_time)
            except: ### no headers , set 15 minutes age , throttling dl
                if global_debug==1:
                    print('HeAders failed: ',url)
                retcode = r.status_code
                ##### get timezone aware datetime object from 900 sec ago
                url_date=pytz.utc.localize(datetime.datetime.fromtimestamp(time.mktime(time.localtime(time.time()-900))))
                #url_date=pytz.utc.localize(datetime.datetime.fromtimestamp(time.mktime(time.localtime(time.time()))))
                pass    
            retcode = r.status_code
            try:
                ##### get timezone aware datetime object
    #            file_time = pytz.utc.localize(date.fromtimestamp(os.path.getmtime(dstFile)))
                file_time = pytz.utc.localize(datetime.datetime.fromtimestamp(os.path.getmtime(dstFile)))
            except:
                ## no file , so 1970-01-01
                file_time=pytz.utc.localize(datetime.datetime.fromtimestamp(time.mktime(time.localtime(0))))
                pass
            # 10 minutes
            cache_time=600         
            if url_date > file_time :
                try:
                    request = Request(url, headers={ 'User-Agent': random.choice(useragents.user_agent_list) })
                    if global_debug==1:
                        print ("Downloading :",url)
                    if urllib_ver2 == 1:
                        #print("urllib2 build opener")
                        opener=urllib2.build_opener()
                    else:
                        #print("urllib.opener v3")
                        opener=urllib.request.build_opener()
                    opener.addheaders = [('User-Agent',random.choice(useragents.user_agent_list))]
                    opened_request = opener.open(request,timeout=15)
                    ##statuscode=opened_request.code
                    if global_debug==1:
                        print ("STATUS_CODE: ",opened_request.code)
                    if opened_request.code == 200:
                        if global_debug==1:
                            print ("start_cache_write: ",dstFile)
                        cacheout = open(dstFile, "wb+")
                        cacheout.write(opened_request.read())
                        cacheout.close()
                    else:
                        return  "ERROR : " + str(e) + ' ' + url     
                except Exception as e:
                    if global_debug==1:
                        print ("ERROR : ",e,url)
                    return  "ERROR : " + str(e) + ' ' + url
            else:
                if global_debug==1:
                    ### we have time-offset aware objects like this : datetime.datetime(2020, 4, 21, 4, 30, 6, tzinfo=tzutc())
                    print ("cache_time: orig :" , url_date.strftime("%d-%b-%Y (%H:%M:%S.%f)") , " local",file_time.strftime("%d-%b-%Y (%H:%M:%S.%f)"))
        else:
            ###we use the cache and it is newer than 10 minute , so we pretend request worked 
            retcode=200    
#    html_content = opened_request.read()
        if global_debug==1:
            print ("start_cache_open : ",dstFile)
        cachefile = open(dstFile, "r")
        html_content = cachefile.read()
        if global_debug==1:
            print ("LIST LEN: ",len(html_content))
        cachefile.close()
        #retcode = opened_request.code
        
        matches = retcode == 200
        matches = matches and re.findall(badip, html_content)
        if validate_ipv4(badip):
            padip='';
            topad = str(badip).split('.')
            for x in topad:
                padip=padip+x.zfill(3)+'.'
            padip=padip.rstrip('.')    
            matches = matches and re.findall(padip, html_content)
        #return len(matches) == 0
        if global_debug==1:
            print ("matches in list: ",len(matches), ' while searching for ' + badip)
        if len(matches) == 0:
            return "passed"
        else:
            return "failed"
    except Exception as e:
        if global_debug==1:
            print('SOMETTHING FAILED while getting http')
            print ("ERROR : ",e,url)
        return "passed"


def check_rbl_thread_runner(argument):
    reverse_target_ip=argument["ip"]
    rbl=argument["rbl"]
    if global_debug==1:
        print('DNS CHECKING',rbl,reverse_target_ip)
    res = _check_rbl(reverse_target_ip, rbl)
    if res is True:
        res = {'rbl': rbl, 'banned': True}
        return res
    elif res is None:
        res = {'rbl': rbl, 'timeout': True}
        return res
    #print(argument)

def check_rbl_threaded(reverse_target_ip,addr_fam=4):
    argumentarray = [];
###### blindly checkin rbl makes no sense as there are senderscore and contact rbls that might not bl
# addr fam decision needs to take place one level above 
    if global_debug==1:
        print(' RBL Argument construction ',reverse_target_ip)

    if addr_fam == 4:
        for rbl in rbls_ipv4:
            argumentarray.append({ "rbl": rbl, "ip":reverse_target_ip })
        if global_debug==1:
            print("Family RBL:",rbls_ipv4)

    if addr_fam == 6:
        for rbl in rbls_ipv6:
            argumentarray.append({ "rbl": rbl, "ip":reverse_target_ip })
        if global_debug==1:
            print("Family RBL:",rbls_ipv6)

    
    if addr_fam == "domain":
        for rbl in rbls_domain:
            argumentarray.append({ "rbl": rbl, "ip":reverse_target_ip })
        if global_debug==1:
            print("Family RBL:",rbls_domain)

    res_list=pool.map(check_rbl_thread_runner, argumentarray)
    #print("thread ing output:",res_list_tmp)
    return res_list

def check_rbl_sequential(reverse_target_ip,addr_fam=4):
    res_list = []

    if addr_fam == 4:
        local_list=rbls_ipv4
    if addr_fam == 6:
        local_list=rbls_ipv6
    if addr_fam == 6:
        local_list=rbls_domain
    
    for rbl in local_list:
        if global_debug==1:
            print('DNS CHECKING',rbl,reverse_target_ip)
        res = _check_rbl(reverse_target_ip, rbl)
        if res is True:
            res = {'rbl': rbl, 'banned': True}
            res_list.append(res)
        elif res is None:
            res = {'rbl': rbl, 'timeout': True}
            res_list.append(res)
    return res_list


#
#def check_rbl(reverse_target_ip):
#    argumentarray = [];
#    validate_ipv4()
#    for rbl in rbls:
#        argumentarray.append({ "rbl": rbl, "ip":reverse_target_ip })
#    res_list_tmp = pool.map(check_rbl_thread_runner, argumentarray)
#    res_list = [] 
#    #print("thread ing output:",res_list_tmp)
#    for val in res_list_tmp: 
#        if val != None : 
#            res_list.append(val) 
#    return res_list
#

def check_rbl_http_singlethread(target):
    res_list = []
    for url, succ, fail, mal in URLS:
        if global_debug==1:
            print(' rbl_http_start:', url)
        content_check=_content_test(url, target)
        if content_check == "passed":
            ### do nothing or print oK
            if global_debug==1:
                print ('OK', target + ' ' + succ )
        else:
            print ('CONTENT-CHECK: ' ,content_check)
            res = {'rbl': fail + ' ' + url , 'banned': True}    
            res_list.append(res)
#        else:
#            res = {'rbl': url + fail, 'timeout': True}
#            res_list.append(res)
    return res_list


def check_rbl_http_runner(incomingarg):
#        for url, succ, fail, mal in URLS:
        url=incomingarg["url"]
        succ=incomingarg["succ"]
        fail=incomingarg["fail"]
        mal=incomingarg["mal"]
        target=incomingarg["target"]
        if global_debug==1:
            print(' rbl_http_start:', url)
        content_check=_content_test(url, target)
        if content_check == "passed":
            ### do nothing or print oK
            if global_debug==1:
                print ('OK', target + ' ' + succ )
        else:
            print ('CONTENT-CHECK: ' ,content_check)
            res = {'rbl': fail + ' ' + url , 'banned': True}    
            return res

def check_rbl_http_multithread(target):
    # _content_test(url, badip):
    argumentarray = [];
    for url, succ, fail, mal in URLS:
        argumentarray.append({ "url": url ,"succ": succ, "fail": fail,"mal":mal ,"target":target})
    pool_http = ThreadPool(6)
    res_list = pool_http.map(check_rbl_http_runner, argumentarray)
    return res_list

def rbl_check_ip(target):
    if global_debug==1:
        print (' testing ',target)
    ##check ipv4 , then   ipRev =  '.'.join( searchIp.split('.')[::-1])
    ip_reverse=dns.reversename.from_address(target).to_text()
    ip_reverse=ip_reverse.rstrip('.').rstrip('.in-addr.arpa').rstrip('.ip6.arpa')
    status_rbl_http=check_rbl_http_multithread(target)
    if global_debug==1:
        print(status_rbl_http)
    if validate_ipv4(target):

        status_rbl=check_rbl_threaded(ip_reverse,4)
    else: ## a ipv6 or domain request
        if global_debug==1:
            print("ipv4 check failed for" , target)
        if validate_ipv6_host_public(target):
                status_rbl=check_rbl_threaded(ip_reverse,6)
        else: ## a domain ends up here (having non-numeric last octet , here a tld, and no : signs)
            if global_debug==1:
                print("ipv6 check failed for" , target)
            status_rbl=check_rbl_threaded(ip_reverse,"domain")

    if global_debug==1:
        print("#######################")
        print(status_rbl)
        print("#######################")

    status_combined=[];
    
    if len(status_rbl) > 0:
        status_combined=status_combined + status_rbl
    if len(status_rbl_http) > 0:
        status_combined=status_combined + status_rbl_http
    status_list = [] 
    for val in status_combined: 
        if val != None : 
            status_list.append(val) 

    return status_list

##tests_
## 127.0.0.2 is always listed says rtfm
#rbl_check_ip('127.0.0.2')
### test some google ipv6
#rbl_check_ip('2a00:1450:400e:809::2003')
